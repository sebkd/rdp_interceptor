"""Декораторы"""

import sys
import logging
import logs.client_log_config
import traceback


def log(func_to_log):
    """Функция-декоратор"""

    def log_saver(*args, **kwargs):
        logger_name = 'server.api' if 'server.py' in sys.argv[0] else 'client.api'
        LOGGER = logging.getLogger(logger_name)

        ret = func_to_log(*args, **kwargs)
        LOGGER.debug(f'Была вызвана функция {func_to_log.__name__} c параметрами {args}, {kwargs}.'
                     f'Вызов из модуля {func_to_log.__module__}.'
                     f'Вызов из функции {traceback.format_stack()[0].strip().split()[-1]}.')

        return ret

    return log_saver
